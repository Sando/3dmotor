cmake_minimum_required(VERSION 3.16)
project(3dmotor)

set(CMAKE_CXX_STANDARD 14)
set(SOURCE_FILES main.cpp)

add_executable(3dmotor_run ${SOURCE_FILES})

target_link_libraries(3dmotor_run 3dmotor_lib)

include_directories(src)
add_subdirectory(src)

add_subdirectory(unit_test)