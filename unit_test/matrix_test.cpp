//
// Created by jamesrapha on 29/08/2020.
//

#include <Matrix3x3.h>
#include <Matrix4x4.h>
#include "gtest/gtest.h"

// Matrix 3x3 Tests
//
class Matrix3x3Fixture : public ::testing::Test {

protected:
    void SetUp() override
    {
        m3 = new Matrix3x3();
    }

    void TearDown() override
    {
        delete m3;
    }

    Matrix3x3 * m3{};
};

TEST_F(Matrix3x3Fixture, Init)
{
    m3 = new Matrix3x3();

    ASSERT_EQ(m3->X, Vector3());
    ASSERT_EQ(m3->Y, Vector3());
    ASSERT_EQ(m3->Z, Vector3());
}

TEST_F(Matrix3x3Fixture, InitXYZ)
{
    Vector3 v1 = Vector3(2.0, 5.0, -10.0);
    Vector3 v2 = Vector3(4.0, 10.0, -7.0);
    Vector3 v3 = Vector3(6.0, 15.0, -4.0);
    m3 = new Matrix3x3(v1, v2, v3);

    ASSERT_EQ(m3->X, v1);
    ASSERT_EQ(m3->Y, v2);
    ASSERT_EQ(m3->Z, v3);
}

TEST_F(Matrix3x3Fixture, InitMaxParam) {
    Vector3 v1 = Vector3(6.0, 3.0, -6.0);
    Vector3 v2 = Vector3(3.1, 7.8, -0.3);
    Vector3 v3 = Vector3(5.3, 3.2, -20.0);
    m3 = new Matrix3x3(6.0, 3.0, -6.0, 3.1, 7.8, -0.3, 5.3, 3.2, -20.0);

    ASSERT_EQ(m3->X, v1);
    ASSERT_EQ(m3->Y, v2);
    ASSERT_EQ(m3->Z, v3);
}

TEST_F(Matrix3x3Fixture, OutStreamMatrix){
    m3 = new Matrix3x3();

    std::ostringstream s1;
    std::ostringstream s2;

    s1 << *m3 << std::endl;
    s2 << "[" << m3->X << "," << std::endl
       << m3->Y << "," << std::endl
       << m3->Z << "]" << std::endl;

    ASSERT_EQ(s1.str(), s2.str());
}

TEST_F(Matrix3x3Fixture, AccessMatrix){
    Matrix3x3 m = Matrix3x3(50.0, -10.0, 12.0, 8.5, 4.8, -6.2, 4.0, 6.0, 50.0);

    ASSERT_EQ(m[0][0], 50.0);
    ASSERT_EQ(m[0][1], -10.0);
    ASSERT_EQ(m[0][2], 12.0);
    ASSERT_EQ(m[1][0], 8.5);
    ASSERT_EQ(m[1][1], 4.8);
    ASSERT_EQ(m[1][2], -6.2);
    ASSERT_EQ(m[2][0], 4.0);
    ASSERT_EQ(m[2][1], 6.0);
    ASSERT_EQ(m[2][2], 50.0);
}

TEST_F(Matrix3x3Fixture, ViolateAccessMatrix){
    Matrix3x3 m = Matrix3x3(50.0, -10.0, 12.0, 8.5, 4.8, -6.2, 4.0, 6.0, 50.0);

    ASSERT_THROW(m[4], std::out_of_range);
    ASSERT_THROW(m[2][4], std::out_of_range);
}

TEST_F(Matrix3x3Fixture, AddMatrix){
    Matrix3x3 m1 = Matrix3x3(4.5, -6.0, 7.2, -10.0, 5.0, -2.0, 0.5, 0.2, -6.0);
    Matrix3x3 m2 = Matrix3x3(8.0, 7.0, -5.0, 5.0, 7.0, 4.5, 0.8, -7.0, -2.0);

    Matrix3x3 mR = Matrix3x3(12.5, 1.0, 2.2, -5.0, 12.0, 2.5, 1.3, -6.8, -8.0);

    ASSERT_EQ(m1 + m2, mR);
}

TEST_F(Matrix3x3Fixture, MinusMatrix){
    Matrix3x3 m1 = Matrix3x3(4.0, 4.0, 4.0, 8.6, -5.0, -9.0, -10.5, 2.0, 0.5);
    Matrix3x3 m2 = Matrix3x3(-5.0, -2.0, -0.5, 50.0, 25.0, 10.0, 4.5, 8.5, -9.0);

    Matrix3x3 mR = Matrix3x3(9.0, 6.0, 4.5, -41.4, -30.0, -19.0, -15.0, -6.5, 9.5);

    ASSERT_EQ(m1 - m2, mR);
}

TEST_F(Matrix3x3Fixture, MultMatrixScal){
    Matrix3x3 m = Matrix3x3(5.5, 4.9, 1.0, 2.0, -5.0, -10.0, 0.5, -50.0, 40.0);

    Matrix3x3 mR = Matrix3x3(16.5, 14.7, 3.0, 6.0, -15.0, -30.0, 1.5, -150.0, 120.0);
    Matrix3x3 mR2 = Matrix3x3(27.5, 24.5, 5.0, 10.0, -25.0, -50.0, 2.5, -250.0, 200.0);

    ASSERT_EQ(3.0 * m, mR);
    ASSERT_EQ(m * 5.0, mR2);
}

TEST_F(Matrix3x3Fixture, MultVectMatrix){
    Matrix3x3 m = Matrix3x3(5.5, 4.9, 1.0, 2.0, -5.0, -10.0, 0.5, -50.0, 40.0);
    Vector3 v = Vector3(-4.0, 4.0, 2.0);

    Vector3 vR = Vector3(-13, -139.6, 36);

    ASSERT_EQ(v * m, vR);
}

TEST_F(Matrix3x3Fixture, DivMatrixScal){
    Matrix3x3 m =  Matrix3x3(16.5, 14.7, 3.0, 6.0, -15.0, -30.0, 1.5, -150.0, 120.0);
    Matrix3x3 mR = Matrix3x3(5.5, 4.9, 1.0, 2.0, -5.0, -10.0, 0.5, -50.0, 40.0);

    ASSERT_EQ(m / 3.0, mR);
}

TEST_F(Matrix3x3Fixture, Div0){
    Matrix3x3 m = Matrix3x3(5.5, 4.9, 1.0, 2.0, -5.0, -10.0, 0.5, -50.0, 40.0);

    ASSERT_THROW(m / 0.0, std::overflow_error);
}

TEST_F(Matrix3x3Fixture, DetMatrix){
    Matrix3x3 m = Matrix3x3(3.0, 6.0, 9.0, 1.0, 1.0, 1.0, 5.0, -10.0, 15.0);

    ASSERT_DOUBLE_EQ(m.det(), -120.0);
}

TEST_F(Matrix3x3Fixture, StaticMatrix){
    ASSERT_EQ(Matrix3x3::Zero(), Matrix3x3(0,0,0,0,0,0,0,0,0));
    ASSERT_EQ(Matrix3x3::Identity(), Matrix3x3(1,0,0,0,1,0,0,0,1));
}

TEST_F(Matrix3x3Fixture, TransposeMatrix){
    Matrix3x3 m = Matrix3x3(5.0, 6.0, -10.0, -2.5, 6.0, 8.0, -50.5, -10.5, 10.5);
    Matrix3x3 mR = Matrix3x3(5.0, -2.5, -50.5, 6.0, 6.0, -10.5, -10.0, 8.0, 10.5);

    ASSERT_EQ(Matrix3x3::Transpose(m), mR);
}
//

// Matrix 4x4 Tests
//
class Matrix4x4Fixture : public ::testing::Test {

protected:
    void SetUp() override
    {
        m4 = new Matrix4x4();
    }

    void TearDown() override
    {
        delete m4;
    }

    Matrix4x4 * m4{};
};

TEST_F(Matrix4x4Fixture, Init)
{
    m4 = new Matrix4x4();

    ASSERT_EQ(m4->X, Vector4(0,0,0,0));
    ASSERT_EQ(m4->Y, Vector4(0,0,0,0));
    ASSERT_EQ(m4->Z, Vector4(0,0,0,0));
    ASSERT_EQ(m4->W, Vector4(0,0,0,0));
}

TEST_F(Matrix4x4Fixture, InitXYZ)
{
    Vector4 v1 = Vector4(2.0, 5.0, -10.0, 4.0);
    Vector4 v2 = Vector4(4.0, 10.0, -7.0, 8.1);
    Vector4 v3 = Vector4(6.0, 15.0, -4.0, 5.0);
    Vector4 v4 = Vector4(-7.0, 4.5, 5.6, 7.8);
    m4 = new Matrix4x4(v1, v2, v3, v4);

    ASSERT_EQ(m4->X, v1);
    ASSERT_EQ(m4->Y, v2);
    ASSERT_EQ(m4->Z, v3);
    ASSERT_EQ(m4->W, v4);
}

TEST_F(Matrix4x4Fixture, InitMaxParam) {
    Vector4 v1 = Vector4(6.0, 3.0, -6.0, -5.0);
    Vector4 v2 = Vector4(3.1, 7.8, -0.3, 2.0);
    Vector4 v3 = Vector4(5.3, 3.2, -20.0, 1.0);
    Vector4 v4 = Vector4(1.0, -7.0, 5.1, 6.2);
    m4 = new Matrix4x4(6.0, 3.0, -6.0, -5.0,
                       3.1, 7.8, -0.3, 2.0,
                       5.3, 3.2, -20.0, 1.0,
                       1.0, -7.0, 5.1, 6.2);

    ASSERT_EQ(m4->X, v1);
    ASSERT_EQ(m4->Y, v2);
    ASSERT_EQ(m4->Z, v3);
    ASSERT_EQ(m4->W, v4);
}

TEST_F(Matrix4x4Fixture, OutStreamMatrix){
    m4 = new Matrix4x4();

    std::ostringstream s1;
    std::ostringstream s2;

    s1 << *m4 << std::endl;
    s2 << "[" << m4->X << "," << std::endl
       << m4->Y << "," << std::endl
       << m4->Z << "," << std::endl
       << m4->W << "]" << std::endl;
    ;

    ASSERT_EQ(s1.str(), s2.str());
}

TEST_F(Matrix4x4Fixture, AccessMatrix){
    Matrix4x4 m = Matrix4x4(50.0, -10.0, 12.0, -0.5,
                            8.5, 4.8, -6.2, -3.0,
                            4.0, 6.0, 50.0, 10.0,
                            -7.2, 5.0, 15.0, -50.0);

    ASSERT_EQ(m[0][0], 50.0);
    ASSERT_EQ(m[0][1], -10.0);
    ASSERT_EQ(m[0][2], 12.0);
    ASSERT_EQ(m[0][3], -0.5);
    ASSERT_EQ(m[1][0], 8.5);
    ASSERT_EQ(m[1][1], 4.8);
    ASSERT_EQ(m[1][2], -6.2);
    ASSERT_EQ(m[1][3], -3.0);
    ASSERT_EQ(m[2][0], 4.0);
    ASSERT_EQ(m[2][1], 6.0);
    ASSERT_EQ(m[2][2], 50.0);
    ASSERT_EQ(m[2][3], 10.0);
    ASSERT_EQ(m[3][0], -7.2);
    ASSERT_EQ(m[3][1], 5.0);
    ASSERT_EQ(m[3][2], 15.0);
    ASSERT_EQ(m[3][3], -50.0);
}

TEST_F(Matrix4x4Fixture, ViolateAccessMatrix){
    Matrix4x4 m = Matrix4x4(50.0, -10.0, 12.0, 7.0,
                            8.5, 4.8, -6.2, -5.0,
                            4.0, 6.0, 50.0, -2.0,
                            15.0, 40.0, 80.0, -20.0);

    ASSERT_THROW(m[4], std::out_of_range);
    ASSERT_THROW(m[2][4], std::out_of_range);
}

TEST_F(Matrix4x4Fixture, AddMatrix){
    Matrix4x4 m1 = Matrix4x4(4.5, -6.0, 7.2, 2.0,
                             -10.0, 5.0, -2.0, -2.0,
                             0.5, 0.2, -6.0, 1.0,
                             -0.5, 2.0, 6.0, -1.0);
    Matrix4x4 m2 = Matrix4x4(8.0, 7.0, -5.0, 4.0,
                             5.0, 7.0, 4.5, -5.0,
                             0.8, -7.0, -2.0, -2.0,
                             6.5, 7.5, 8.0, 0.0);

    Matrix4x4 mR = Matrix4x4(12.5, 1.0, 2.2, 6.0,
                             -5.0, 12.0, 2.5, -7.0,
                             1.3, -6.8, -8.0, -1.0,
                             6.0, 9.5, 14.0, -1.0);

    ASSERT_EQ(m1 + m2, mR);
}

TEST_F(Matrix4x4Fixture, MinusMatrix){
    Matrix4x4 m1 = Matrix4x4(4.0, 4.0, 4.0, 4.0,
                             8.6, -5.0, -9.0, -2.0,
                             -10.5, 2.0, 0.5, -1.0,
                             10.0, 5.0, 0.0, 1.0);
    Matrix4x4 m2 = Matrix4x4(-5.0, -2.0, -0.5, 5.0,
                             50.0, 25.0, 10.0, 15.0,
                             4.5, 8.5, -9.0, -15.0,
                             0.2, 0.4, 0.6, 0.8);

    Matrix4x4 mR = Matrix4x4(9.0, 6.0, 4.5, -1.0,
                             -41.4, -30.0, -19.0, -17.0,
                             -15.0, -6.5, 9.5, 14.0,
                             9.8, 4.6, -0.6, 0.2);

    Matrix4x4 minus = m1 - m2;

    ASSERT_EQ(m1 - m2, mR);
}

TEST_F(Matrix4x4Fixture, MultMatrixScal){
    Matrix4x4 m = Matrix4x4(5.5, 4.9, 1.0, 5.5,
                            2.0, -5.0, -10.0, 0.2,
                            0.5, -50.0, 40.0, 1.0,
                            -5.0, -6.0, -7.0, 8.0);

    Matrix4x4 mR = Matrix4x4(16.5, 14.7, 3.0, 16.5,
                             6.0, -15.0, -30.0, 0.6,
                             1.5, -150.0, 120.0, 3.0,
                             -15.0, -18.0, -21.0, 24.0);
    Matrix4x4 mR2 = Matrix4x4(27.5, 24.5, 5.0, 27.5,
                              10.0, -25.0, -50.0, 1.0,
                              2.5, -250.0, 200.0, 5.0,
                              -25.0, -30.0, -35.0, 40.0);

    ASSERT_EQ(3.0 * m, mR);
    ASSERT_EQ(m * 5.0, mR2);
}

TEST_F(Matrix4x4Fixture, MultVectMatrix){
    Matrix4x4 m = Matrix4x4(5.5, 4.9, 1.0, -5.0,
                            2.0, -5.0, -10.0, 5.0,
                            0.5, -50.0, 40.0, 2.0,
                            -10.0, 5.0, 20.0, -2.0);
    Vector4 v = Vector4(-4.0, 4.0, 2.0, 1.0);

    Vector4 vR = Vector4(-23, -134.6, 56, 42);

    ASSERT_EQ(v * m, vR);
}

TEST_F(Matrix4x4Fixture, DivMatrixScal){
    Matrix4x4 m =  Matrix4x4(16.5, 14.7, 3.0, 9.3,
                             6.0, -15.0, -30.0, 12.6,
                             1.5, -150.0, 120.0, 21.9,
                             15.0, 3.21, 210.0, 42.0);
    Matrix4x4 mR = Matrix4x4(5.5, 4.9, 1.0, 3.1,
                             2.0, -5.0, -10.0, 4.2,
                             0.5, -50.0, 40.0, 7.3,
                             5.0, 1.07, 70.0, 14.0);

    ASSERT_EQ(m / 3.0, mR);
}

TEST_F(Matrix4x4Fixture, Div0){
    Matrix4x4 m = Matrix4x4(5.5, 4.9, 1.0, 5.0,
                            2.0, -5.0, -10.0, -5.0,
                            0.5, -50.0, 40.0, -20.0,
                            5.5, 10.5, 20.5, 50.5);

    ASSERT_THROW(m / 0.0, std::overflow_error);
}

TEST_F(Matrix4x4Fixture, DetMatrix){
    Matrix4x4 m = Matrix4x4(3.0, 6.0, 9.0, 5.5,
                            1.0, 1.0, 1.0, 1.0,
                            5.0, -10.0, 15.0, 2.0,
                            -0.5, -50.0, 10.0, 5.0);

    ASSERT_DOUBLE_EQ(m.det(), -2489.25);
}

TEST_F(Matrix4x4Fixture, StaticMatrix){
    ASSERT_EQ(Matrix4x4::Zero(), Matrix4x4(0,0,0,0,
                                           0,0,0,0,
                                           0,0,0,0,
                                           0,0,0,0));
    ASSERT_EQ(Matrix4x4::Identity(), Matrix4x4(1,0,0,0,
                                               0,1,0,0,
                                               0,0,1,0,
                                               0,0,0,1));
}

TEST_F(Matrix4x4Fixture, TransposeMatrix){
    Matrix4x4 m = Matrix4x4(5.0, 6.0, -10.0, -5.0,
                            -2.5, 6.0, 8.0, 10.0,
                            -50.5, -10.5, 10.5,0.4,
                            50.0, 4.0, -8.0, 5.5);
    Matrix4x4 mR = Matrix4x4(5.0, -2.5, -50.5, 50.0,
                             6.0, 6.0, -10.5, 4.0,
                             -10.0, 8.0, 10.5, -8.0,
                             -5.0, 10.0, 0.4, 5.5);

    ASSERT_EQ(Matrix4x4::Transpose(m), mR);
}
//