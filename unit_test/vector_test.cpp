//
// Created by jamesrapha on 26/07/2020.
//

#include <cmath>
#include <vector3.h>
#include <vector4.h>
#include "gtest/gtest.h"


// Vector4 Tests
//
class Vector4Fixture : public ::testing::Test {

protected:
    void SetUp() override
    {
        v4 = new Vector4();
    }

    void TearDown() override {
        delete v4;
    }

    Vector4 * v4{};
};

TEST_F(Vector4Fixture, Init){ // 1/1/1 -> 1/3/1

    v4 = new Vector4();

    EXPECT_EQ(v4->x,0.0);
    EXPECT_EQ(v4->y,0.0);
    EXPECT_EQ(v4->z,0.0);
    EXPECT_EQ(v4->w,1.0);
}

TEST_F(Vector4Fixture, InitXYZ){
    v4 = new Vector4(3.2, 2.2, 6.5);

    EXPECT_EQ(v4->x,3.2);
    EXPECT_EQ(v4->y,2.2);
    EXPECT_EQ(v4->z,6.5);
    EXPECT_EQ(v4->w,1.0);
}

TEST_F(Vector4Fixture, InitXYZW){
    v4 = new Vector4(-58.4, 46.4, 18.0, 0.5);

    EXPECT_EQ(v4->x,-58.4);
    EXPECT_EQ(v4->y,46.4);
    EXPECT_EQ(v4->z,18.0);
    EXPECT_EQ(v4->w,0.5);
}

TEST_F(Vector4Fixture, AccessV4){
    Vector4 v = Vector4(3.4, 6.2, -7.3);

    ASSERT_EQ(v[0], 3.4);
    ASSERT_EQ(v[1], 6.2);
    ASSERT_EQ(v[2], -7.3);
    ASSERT_EQ(v[3], 1.0);
}

TEST_F(Vector4Fixture, ViolateAccessV4){
    Vector4 v = Vector4();
    ASSERT_THROW(v[-2], std::out_of_range);
    //ASSERT_EXIT(v[-2], ::testing::ExitedWithCode(EXIT_FAILURE), "Error: Out of range Vector4 access");
}

TEST_F(Vector4Fixture, OppositeV4){
    Vector4 vSauce = Vector4(-58.4, 46.4, 18.0, 0.5);
    Vector4 vSauceOpp = -vSauce;

    EXPECT_EQ(vSauceOpp.x,58.4);
    EXPECT_EQ(vSauceOpp.y,-46.4);
    EXPECT_EQ(vSauceOpp.z,-18.0);
    EXPECT_EQ(vSauceOpp.w,0.5);
}

TEST_F(Vector4Fixture, MinusV4){
    Vector4 v1 = Vector4(5.2, 6.4, 7.5, 1.0);
    Vector4 v2 = Vector4(-2.0, 6.4, 34.3, 1.0);

    Vector4 minus = v1 - v2;
    Vector4 minusR = Vector4(7.2, 0.0, -26.8, 0.0);

    EXPECT_EQ(minus, minusR);
}

TEST_F(Vector4Fixture, AddV4){
    Vector4 v1 = Vector4(5.2, 6.4, 7.5, 1.0);
    Vector4 v2 = Vector4(-2.0, 6.4, 34.3, 1.0);

    Vector4 sum = v1 + v2;

    EXPECT_EQ(sum.x,3.2);
    EXPECT_EQ(sum.y,12.8);
    EXPECT_EQ(sum.z,41.8);
    EXPECT_EQ(sum.w,2.0);
}

TEST_F(Vector4Fixture, OutStreamV4){
    Vector4 v = Vector4(-2.0, 6.4, 34.3, 1.0);
    std::ostringstream s1;
    s1 << v;

    ASSERT_EQ(s1.str(),  "(-2, 6.4, 34.3, 1)");
}

TEST_F(Vector4Fixture, MultScalarV4){
    Vector4 v = Vector4(4.3, 8.2, -23.3);

    ASSERT_EQ(v * 4.0, Vector4(17.2, 32.8, -93.2, 4.0));
    ASSERT_EQ(-3.0 * v, Vector4(-12.9, -24.6, 69.9, -3.0));
}

TEST_F(Vector4Fixture, DivScalarV4){
    Vector4 v = Vector4(33.0, 99.0, -12.0);

    ASSERT_EQ(v / 3.0, Vector4(11.0, 33.0, -4.0, 1.0 / 3.0));
}

TEST_F(Vector4Fixture, DotV4){
    Vector4 v1 = Vector4(33.0, 99.0, -12.0);
    Vector4 v2 = Vector4(-2.0, 6.4, 34.3);

    ASSERT_DOUBLE_EQ(Vector4::Dot(v1, v2), 156.0);
}

TEST_F(Vector4Fixture, CrossV4){
    Vector4 v1 = Vector4(3.4, 7.4, -2.0);
    Vector4 v2 = Vector4(6.4, -2.4, 5.2);
    Vector4 v3 = Vector4(33.68, -30.48, -55.52);

    ASSERT_EQ(Vector4::Cross(v1, v2), v3);
}

TEST_F(Vector4Fixture, Sqr){
    Vector4 v = Vector4(5.6, -4.8, 10.5);

    ASSERT_DOUBLE_EQ(v.sqr(), 165.65);
}

TEST_F(Vector4Fixture, Mag){
    Vector4 v = Vector4(5.0, 4.0, -6.0);

    ASSERT_DOUBLE_EQ(v.mag(), sqrt(78.0));
}

TEST_F(Vector4Fixture, Normalize){
    Vector4 v = Vector4(5.0, 3.0, 10.0, 5.0);
    Vector4 vComp = Vector4(5.0 / sqrt(159), 3.0 / sqrt(159), 10.0 / sqrt(159), 5.0 / sqrt(159));

    Vector4 vNull = Vector4(0.0, 0.0, 0.0, 0.0);

    ASSERT_EQ(Vector4::Normalize(v), vComp);
    ASSERT_THROW(Vector4::Normalize(vNull), std::invalid_argument);
}

TEST_F(Vector4Fixture, Distance){
    Vector4 v1 = Vector4(3.0, 5.6, -4.0);
    Vector4 v2 = Vector4(4.8, -4.5, 10.0);

    ASSERT_DOUBLE_EQ(Vector4::Distance(v1, v2), sqrt(301.25));
}
// Vector4 Tests

// Vector3 Tests
//
class Vector3Fixture : public ::testing::Test {

protected:
    void SetUp() override
    {
        v3 = new Vector3();
    }

    void TearDown() override {
        delete v3;
    }

    Vector3 * v3{};
};

TEST_F(Vector3Fixture, Init){ // 1/1/1 -> 1/3/1

    v3 = new Vector3();

    EXPECT_EQ(v3->x,0.0);
    EXPECT_EQ(v3->y,0.0);
    EXPECT_EQ(v3->z,0.0);
}

TEST_F(Vector3Fixture, InitXYZ){
    v3 = new Vector3(3.2, 2.2, 6.5);

    EXPECT_EQ(v3->x,3.2);
    EXPECT_EQ(v3->y,2.2);
    EXPECT_EQ(v3->z,6.5);
}

TEST_F(Vector3Fixture, InitXYZW){
    v3 = new Vector3(-58.4, 46.4, 18.0);

    EXPECT_EQ(v3->x,-58.4);
    EXPECT_EQ(v3->y,46.4);
    EXPECT_EQ(v3->z,18.0);
}

TEST_F(Vector3Fixture, AccessV4){
    Vector3 v = Vector3(3.4, 6.2, -7.3);

    ASSERT_EQ(v[0], 3.4);
    ASSERT_EQ(v[1], 6.2);
    ASSERT_EQ(v[2], -7.3);
}

TEST_F(Vector3Fixture, ViolateAccessV4){
    Vector3 v = Vector3();
    ASSERT_THROW(v[-2], std::out_of_range);
    //ASSERT_EXIT(v[-2], ::testing::ExitedWithCode(EXIT_FAILURE), "Error: Out of range Vector3 access");
}

TEST_F(Vector3Fixture, OppositeV4){
    Vector3 vSauce = Vector3(-58.4, 46.4, 18.0);
    Vector3 vSauceOpp = -vSauce;

    EXPECT_EQ(vSauceOpp.x,58.4);
    EXPECT_EQ(vSauceOpp.y,-46.4);
    EXPECT_EQ(vSauceOpp.z,-18.0);
}

TEST_F(Vector3Fixture, MinusV4){
    Vector3 v1 = Vector3(5.2, 6.4, 7.5);
    Vector3 v2 = Vector3(-2.0, 6.4, 34.3);

    Vector3 sum = v1 + v2;

    EXPECT_EQ(sum.x,3.2);
    EXPECT_EQ(sum.y,12.8);
    EXPECT_EQ(sum.z,41.8);
}

TEST_F(Vector3Fixture, AddV4){
    Vector3 v1 = Vector3(5.2, 6.4, 7.5);
    Vector3 v2 = Vector3(-2.0, 6.4, 34.3);

    Vector3 sum = v1 + v2;

    EXPECT_EQ(sum.x,3.2);
    EXPECT_EQ(sum.y,12.8);
    EXPECT_EQ(sum.z,41.8);
}

TEST_F(Vector3Fixture, OutStreamV4){
    Vector3 v = Vector3(-2.0, 6.4, 34.3);
    std::ostringstream s1;
    s1 << v;

    ASSERT_EQ(s1.str(),  "(-2, 6.4, 34.3)");
}

TEST_F(Vector3Fixture, MultScalarV4){
    Vector3 v = Vector3(4.3, 8.2, -23.3);

    ASSERT_EQ(v * 4.0, Vector3(17.2, 32.8, -93.2));
    ASSERT_EQ(-3.0 * v, Vector3(-12.9, -24.6, 69.9));
}

TEST_F(Vector3Fixture, DivScalarV4){
    Vector3 v = Vector3(33.0, 99.0, -12.0);

    ASSERT_EQ(v / 3.0, Vector3(11.0, 33.0, -4.0));
}

TEST_F(Vector3Fixture, DotV4){
    Vector3 v1 = Vector3(33.0, 99.0, -12.0);
    Vector3 v2 = Vector3(-2.0, 6.4, 34.3);

    ASSERT_DOUBLE_EQ(Vector3::Dot(v1, v2), 156.0);
}

TEST_F(Vector3Fixture, CrossV4){
    Vector3 v1 = Vector3(3.4, 7.4, -2.0);
    Vector3 v2 = Vector3(6.4, -2.4, 5.2);
    Vector3 v3 = Vector3(33.68, -30.48, -55.52);

    ASSERT_EQ(Vector3::Cross(v1, v2), v3);
}

TEST_F(Vector3Fixture, Sqr){
    Vector3 v = Vector3(5.6, -4.8, 10.5);

    ASSERT_DOUBLE_EQ(v.sqr(), 164.65);
}

TEST_F(Vector3Fixture, Mag){
    Vector3 v = Vector3(5.0, 4.0, -6.0);

    ASSERT_DOUBLE_EQ(v.mag(), sqrt(77.0));
}

TEST_F(Vector3Fixture, Normalize){
    Vector3 v = Vector3(12.2, 5.5, -6.0);
    Vector3 vComp = Vector3(12.2 / sqrt(215.09), 5.5 / sqrt(215.09), -6.0 / sqrt(215.09));

    Vector3 vNull = Vector3(0.0, 0.0, 0.0);

    ASSERT_EQ(Vector3::Normalize(v), vComp);
    ASSERT_THROW(Vector3::Normalize(vNull), std::invalid_argument);
}

TEST_F(Vector3Fixture, Distance){
    Vector3 v1 = Vector3(1.6, 2.8, 9.2);
    Vector3 v2 = Vector3(-7.5, -8.1, 4.6);

    ASSERT_DOUBLE_EQ(Vector3::Distance(v1, v2), sqrt(222.78));
}
//