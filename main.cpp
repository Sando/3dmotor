#include "iostream"
#include "vector4.h"
int main (int argc, char *argv[]) {
    std::cout << "Hello, World!" << std::endl;
    Vector4 a = Vector4();
    Vector4 b = Vector4(5.0, 4.65, -12.0);

    Vector4 c = a - b;
    std::cout << c << std::endl;

    return 0;
}