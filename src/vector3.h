//
// Created by jamesrapha on 26/07/2020.
//

#ifndef INC_3DMOTOR_VECTOR3_H
#define INC_3DMOTOR_VECTOR3_H

#include <ostream>

class Vector3 {

public:
    double x;
    double y;
    double z;

    Vector3();
    Vector3(double x, double y);
    Vector3(double x, double y, double z);

    virtual ~Vector3();

    Vector3& operator=(Vector3 const&v);
    double & operator[](int i);

    friend bool operator==(const Vector3 &lhs, const Vector3 &rhs);
    friend bool operator!=(const Vector3 &lhs, const Vector3 &rhs);

    friend std::ostream &operator<<(std::ostream &os, const Vector3 &Vector3);
    friend Vector3 operator-(const Vector3 &lhs, const Vector3 &rhs);
    friend Vector3 operator+(const Vector3 &lhs, const Vector3 &rhs);
    friend Vector3 operator*(const double &lhs, const Vector3 &rhs);
    friend Vector3 operator*(const Vector3 &lhs, const double &rhs);
    friend Vector3 operator*(const Vector3 &lhs, const Vector3 &rhs);
    friend Vector3 operator/(const Vector3 &lhs, const double &rhs);

    static double Dot(const Vector3 &dhs, const Vector3 &rhs);
    static Vector3 Cross(const Vector3 &dhs, const Vector3 &rhs);
    static Vector3 Normalize(const Vector3 &v);
    static double Distance(const Vector3 &lhs, const Vector3 &rhs);

    Vector3 operator-() const;
    double sqr() const;
    double mag() const;

};

#endif //INC_3DMOTOR_VECTOR3_H