//
// Created by jamesrapha on 29/08/2020.
//

#include "Matrix3x3.h"

Matrix3x3::Matrix3x3() : X(Vector3()), Y(Vector3()), Z(Vector3()){}

Matrix3x3::Matrix3x3(Vector3 X, Vector3 Y, Vector3 Z) : X(X), Y(Y), Z(Z) {}

Matrix3x3::Matrix3x3(double x1, double x2, double x3,
                     double y1, double y2, double y3,
                     double z1, double z2, double z3) :
                     X(Vector3(x1, x2, x3)), Y(Vector3(y1, y2, y3)), Z(Vector3(z1, z2, z3)){}


Vector3 &Matrix3x3::operator[](int i) {
    switch(i) {
        case 0:
            return this->X;
        case 1:
            return this->Y;
        case 2:
            return this->Z;
        default:
            throw std::out_of_range("Error: Out of range Matrix3x3 access");
    }
}

bool Matrix3x3::operator==(const Matrix3x3 &rhs) const {
    /*
    bool xTruth = X == rhs.X;
    bool yTruth = Y == rhs.Y;
    bool zTruth = Z == rhs.Z;

    return xTruth && yTruth && zTruth;
    */
    return X == rhs.X &&
           Y == rhs.Y &&
           Z == rhs.Z;
}

bool Matrix3x3::operator!=(const Matrix3x3 &rhs) const {
    return !(rhs == *this);
}


std::ostream &operator<<(std::ostream &os, const Matrix3x3 &matrix3x3) {
    return os << "[" << matrix3x3.X << "," << std::endl
                     << matrix3x3.Y << "," << std::endl
                     << matrix3x3.Z << "]";
}



Matrix3x3 operator+(const Matrix3x3 &lhs, const Matrix3x3 &rhs) {
    return Matrix3x3(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);
}

Matrix3x3 operator-(const Matrix3x3 &lhs, const Matrix3x3 &rhs) {
    return Matrix3x3(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z);
}

Matrix3x3 operator*(const double &lhs, const Matrix3x3 &rhs) {
    return Matrix3x3(lhs * rhs.X, lhs * rhs.Y, lhs * rhs.Z);
}

Matrix3x3 operator*(const Matrix3x3 &lhs, const double &rhs) {
    return rhs * lhs;
}

Vector3 operator*(const Vector3 &lhs, const Matrix3x3 &rhs) {
    return Vector3(lhs.x * rhs.X.x + lhs.y * rhs.Y.x + lhs.z * rhs.Z.x,
                   lhs.x * rhs.X.y + lhs.y * rhs.Y.y + lhs.z * rhs.Z.y,
                   lhs.x * rhs.X.z + lhs.y * rhs.Y.z + lhs.z * rhs.Z.z);
}

Matrix3x3 operator/(const Matrix3x3 &lhs, const double &rhs) {
    if(rhs == 0)
        throw std::overflow_error("Division by 0 forbidden");
    return Matrix3x3(lhs.X / rhs, lhs.Y / rhs, lhs.Z / rhs);
}

double Matrix3x3::det() const {
    return this->X.x * (this->Y.y * this->Z.z - this->Z.y * this->Y.z)
        + this->X.y * (this->Y.z * this->Z.x - this->Z.z * this->Y.x)
        + this->X.z * (this->Y.x * this->Z.y - this->Z.x * this->Y.y);
}

Matrix3x3 Matrix3x3::Zero() {
    return Matrix3x3();
}

Matrix3x3 Matrix3x3::Identity() {
    return Matrix3x3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
}

Matrix3x3 Matrix3x3::Transpose(const Matrix3x3& m) {
    return Matrix3x3(m.X.x, m.Y.x, m.Z.x, m.X.y, m.Y.y, m.Z.y, m.X.z, m.Y.z, m.Z.z);
}


Matrix3x3::~Matrix3x3() = default;