//
// Created by jamesrapha on 25/08/2020.
//

#include "vector3.h"
#include <cmath>
#include <cfloat>

Vector3::Vector3() : x(0), y(0), z(0) {}

Vector3::Vector3(double x, double y) : x(x), y(y), z(0) {}

Vector3::Vector3(double x, double y, double z) : x(x), y(y), z(z) {}

Vector3::~Vector3() = default;

Vector3 &Vector3::operator=(const Vector3 &v) = default;

double &Vector3::operator[](int i) {
    switch(i) {
        case 0:
            return this->x;
        case 1:
            return this->y;
        case 2:
            return this->z;
        default:
            throw std::out_of_range("Error: Out of range Vector3 access");
    }
}

bool operator==(const Vector3 &lhs, const Vector3 &rhs) {
/*
    bool xTruth = std::fabs(lhs.x - rhs.x) <= DBL_EPSILON * std::fabs(rhs.x+lhs.x);
    bool yTruth = std::fabs(lhs.y - rhs.y) <= DBL_EPSILON * std::fabs(rhs.y+lhs.y);
    bool zTruth = std::fabs(lhs.z - rhs.z) <= DBL_EPSILON * std::fabs(rhs.z+lhs.z);

    return xTruth && yTruth && zTruth;
*/
    return std::fabs(lhs.x - rhs.x) <= DBL_EPSILON * std::fabs(rhs.x+lhs.x) &&
           std::fabs(lhs.y - rhs.y) <= DBL_EPSILON * std::fabs(rhs.y+lhs.y) &&
           std::fabs(lhs.z - rhs.z) <= DBL_EPSILON * std::fabs(rhs.z+lhs.z);
}

bool operator!=(const Vector3 &lhs, const Vector3 &rhs) {
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, const Vector3 &vector3) {
    os << "(" << vector3.x << ", " << vector3.y << ", " << vector3.z << ")";
    return os;
}

Vector3 operator-(const Vector3 &lhs, const Vector3 &rhs) {
    return Vector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}

Vector3 operator+(const Vector3 &lhs, const Vector3 &rhs) {
    return Vector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

Vector3 operator*(const double &lhs, const Vector3 &rhs) {
    return Vector3(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
}

Vector3 operator*(const Vector3 &lhs, const double &rhs) {
    return rhs * lhs;
}

Vector3 operator*(const Vector3 &lhs, const Vector3 &rhs) {
    return Vector3(lhs.x * rhs.x + lhs.y * rhs.x + lhs.z * rhs.x,
                   lhs.x * rhs.y + lhs.y * rhs.y + lhs.z * rhs.y,
                   lhs.x * rhs.z + lhs.y * rhs.z + lhs.z * rhs.z);
}

Vector3 operator/(const Vector3 &lhs, const double &rhs) {
    return Vector3(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
}

double Vector3::Dot(const Vector3 &dhs, const Vector3 &rhs) {
    return dhs.x * rhs.x + dhs.y * rhs.y + dhs.z * rhs.z;
}

Vector3 Vector3::Cross(const Vector3 &dhs, const Vector3 &rhs) {
    return Vector3(dhs.y * rhs.z - dhs.z * rhs.y,
                   dhs.z * rhs.x - dhs.x * rhs.z,
                   dhs.x * rhs.y - dhs.y * rhs.x);}

Vector3 Vector3::Normalize(const Vector3 &v) {
    if(v.x == 0.0 && v.y == 0.0 && v.z == 0.0)
        throw std::invalid_argument("Can't normalize null vector");
    return v / v.mag();}

double Vector3::Distance(const Vector3 &lhs, const Vector3 &rhs) {
    return (rhs - lhs).mag();
}

Vector3 Vector3::operator-() const {
    return Vector3(-this->x, -this->y, -this->z);
}

double Vector3::sqr() const {
    return this->x * this->x + this->y * this->y + this->z * this->z;
}

double Vector3::mag() const {
    return std::sqrt(this->sqr());
}
