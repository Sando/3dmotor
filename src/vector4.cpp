//
// Created by jamesrapha on 26/07/2020.
//

#include "vector4.h"
#include <cmath>
#include <iostream>
#include <cfloat>

Vector4::Vector4() : x(0), y(0), z(0), w(1.0) {}
Vector4::Vector4(double x, double y, double z) : x(x), y(y), z(z), w(1.0) {}
Vector4::Vector4(double x, double y, double z, double w) : x(x), y(y), z(z), w(w) {}

Vector4::~Vector4() = default;

bool operator==(const Vector4 &lhs, const Vector4 &rhs) {
    return std::fabs(lhs.x - rhs.x) <= DBL_EPSILON * std::fabs(rhs.x+lhs.x) &&
           std::fabs(lhs.y - rhs.y) <= DBL_EPSILON * std::fabs(rhs.y+lhs.y) &&
           std::fabs(lhs.z - rhs.z) <= DBL_EPSILON * std::fabs(rhs.z+lhs.z) &&
           std::fabs(lhs.w - rhs.w) <= DBL_EPSILON * std::fabs(rhs.w+lhs.w);
}

bool operator!=(const Vector4 &lhs, const Vector4 &rhs) {
    return !(rhs == lhs);
}

std::ostream &operator<<(std::ostream &os, const Vector4 &vector4) {
    os << "(" << vector4.x << ", " << vector4.y << ", " << vector4.z << ", " << vector4.w << ")";
    return os;
}

Vector4 operator-(const Vector4 &lhs, const Vector4 &rhs) {
    return Vector4(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w);
}

Vector4 operator+(const Vector4 &lhs, const Vector4 &rhs) {
    return Vector4(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
}


Vector4& Vector4::operator=(Vector4 const&v) = default;


double &Vector4::operator[](int i) {
    switch(i){
        case 0:
            return this->x;
        case 1:
            return this->y;
        case 2:
            return this->z;
        case 3:
            return this->w;
        default:
            throw std::out_of_range("Error: Out of range Vector4 access");
    }
}

Vector4 operator*(const double &lhs, const Vector4 &rhs) {
    return Vector4(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w);
}

Vector4 operator*(const Vector4 &lhs, const double &rhs) {
    return rhs * lhs;
}

Vector4 operator/(const Vector4 &lhs, const double &rhs) {
    return Vector4(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs);
}


Vector4 Vector4::operator-() const {
    return Vector4(-this->x, -this->y, -this->z, this->w);
}

double Vector4::Dot(const Vector4 &dhs, const Vector4 &rhs) {
    return dhs.x * rhs.x + dhs.y * rhs.y + dhs.z * rhs.z;
}

Vector4 Vector4::Cross(const Vector4 &dhs, const Vector4 &rhs) {
    return Vector4(dhs.y * rhs.z - dhs.z * rhs.y,
                   dhs.z * rhs.x - dhs.x * rhs.z,
                   dhs.x * rhs.y - dhs.y * rhs.x);
}

Vector4 Vector4::Normalize(const Vector4 &v) {
    if(v.x == 0.0 && v.y == 0.0 && v.z == 0.0 && v.w == 0.0)
        throw std::invalid_argument("Can't normalize null vector");
    return v / v.mag();
}

double Vector4::sqr() const {
    return this->x * this->x + this->y * this->y + this->z * this->z + this->w * this->w;
}

double Vector4::mag() const {
    return std::sqrt(this->sqr());
}

double Vector4::Distance(const Vector4 &lhs, const Vector4 &rhs) {
    return (rhs - lhs).mag();
}






