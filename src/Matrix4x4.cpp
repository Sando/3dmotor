//
// Created by jamesrapha on 29/08/2020.
//

#include "Matrix4x4.h"
#include "Matrix3x3.h"

Matrix4x4::Matrix4x4() : X(Vector4(0,0,0,0)),
                         Y(Vector4(0,0,0,0)),
                         Z(Vector4(0,0,0,0)),
                         W(Vector4(0,0,0,0)) {}

Matrix4x4::~Matrix4x4() = default;

Matrix4x4::Matrix4x4(const Vector4& X, const Vector4& Y, const Vector4& Z, const Vector4& W) : X(X), Y(Y), Z(Z), W(W) {}

Matrix4x4::Matrix4x4(double x1, double x2, double x3, double x4,
                     double y1, double y2, double y3, double y4,
                     double z1, double z2, double z3, double z4,
                     double w1, double w2, double w3, double w4) :
                     X(Vector4(x1, x2, x3, x4)),
                     Y(Vector4(y1, y2, y3, y4)),
                     Z(Vector4(z1, z2, z3, z4)),
                     W(Vector4(w1, w2, w3, w4)) {}

Vector4 &Matrix4x4::operator[](int i) {
    switch(i) {
        case 0:
            return this->X;
        case 1:
            return this->Y;
        case 2:
            return this->Z;
        case 3:
            return this->W;
        default:
            throw std::out_of_range("Error: Out of range Matrix4x4 access");
    }
}

bool Matrix4x4::operator==(const Matrix4x4 &rhs) const {
    /*
    bool xTruth = X == rhs.X;
    bool yTruth = Y == rhs.Y;
    bool zTruth = Z == rhs.Z;
     bool wTruth = W == rhs.W;

    return xTruth && yTruth && zTruth && wTruth;
    */
    return X == rhs.X &&
           Y == rhs.Y &&
           Z == rhs.Z &&
           W == rhs.W;
}

bool Matrix4x4::operator!=(const Matrix4x4 &rhs) const {
    return !(rhs == *this);
}


std::ostream &operator<<(std::ostream &os, const Matrix4x4 &matrix4x4) {
    return os << "[" << matrix4x4.X << "," << std::endl
              << matrix4x4.Y << "," << std::endl
              << matrix4x4.Z << "," << std::endl
              << matrix4x4.W << "]";
}


Matrix4x4 operator+(const Matrix4x4 &lhs, const Matrix4x4 &rhs) {
    return Matrix4x4(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z, lhs.W + rhs.W);
}

Matrix4x4 operator-(const Matrix4x4 &lhs, const Matrix4x4 &rhs) {
    return Matrix4x4(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z, lhs.W - rhs.W);
}

Matrix4x4 operator*(const double &lhs, const Matrix4x4 &rhs) {
    return Matrix4x4(lhs * rhs.X, lhs * rhs.Y, lhs * rhs.Z, lhs * rhs.W);
}

Matrix4x4 operator*(const Matrix4x4 &lhs, const double &rhs) {
    return rhs * lhs;
}

Vector4 operator*(const Vector4 &lhs, const Matrix4x4 &rhs) {
    return Vector4(lhs.x * rhs.X.x + lhs.y * rhs.Y.x + lhs.z * rhs.Z.x + lhs.w * rhs.W.x,
                   lhs.x * rhs.X.y + lhs.y * rhs.Y.y + lhs.z * rhs.Z.y + lhs.w * rhs.W.y,
                   lhs.x * rhs.X.z + lhs.y * rhs.Y.z + lhs.z * rhs.Z.z + lhs.w * rhs.W.z,
                   lhs.x * rhs.X.w + lhs.y * rhs.Y.w + lhs.z * rhs.Z.w + lhs.w * rhs.W.w);
}

Matrix4x4 operator/(const Matrix4x4 &lhs, const double &rhs) {
    if(rhs == 0)
        throw std::overflow_error("Division by 0 forbidden");
    return Matrix4x4(lhs.X / rhs, lhs.Y / rhs, lhs.Z / rhs, lhs.W / rhs);
}

double Matrix4x4::det() const {
    Matrix3x3 a1;
    Matrix3x3 a2;
    Matrix3x3 a3;
    Matrix3x3 a4;

    a1 = Matrix3x3(this->Y.y, this->Y.z, this->Y.w,
                  this->Z.y, this->Z.z, this->Z.w,
                  this->W.y, this->W.z, this->W.w);
    a2 = Matrix3x3(this->Y.x, this->Y.z, this->Y.w,
                   this->Z.x, this->Z.z, this->Z.w,
                   this->W.x, this->W.z, this->W.w);
    a3 = Matrix3x3(this->Y.x, this->Y.y, this->Y.w,
                   this->Z.x, this->Z.y, this->Z.w,
                   this->W.x, this->W.y, this->W.w);
    a4 = Matrix3x3(this->Y.x, this->Y.y, this->Y.z,
                   this->Z.x, this->Z.y, this->Z.z,
                   this->W.x, this->W.y, this->W.z);

    return this->X.x * a1.det()
           - this->X.y * a2.det()
           + this->X.z * a3.det()
           - this->X.w * a4.det();
}

Matrix4x4 Matrix4x4::Zero() {
    return Matrix4x4();
}

Matrix4x4 Matrix4x4::Identity() {
    return Matrix4x4(1.0, 0.0, 0.0, 0.0,
                     0.0, 1.0, 0.0, 0.0,
                     0.0, 0.0, 1.0, 0.0,
                     0.0, 0.0, 0.0, 1.0);
}

Matrix4x4 Matrix4x4::Transpose(const Matrix4x4& m) {
    return Matrix4x4(m.X.x, m.Y.x, m.Z.x, m.W.x,
                     m.X.y, m.Y.y, m.Z.y, m.W.y,
                     m.X.z, m.Y.z, m.Z.z, m.W.z,
                     m.X.w, m.Y.w, m.Z.w, m.W.w);
}