//
// Created by jamesrapha on 26/07/2020.
//

#ifndef INC_3DMOTOR_VECTOR4_H
#define INC_3DMOTOR_VECTOR4_H

#include <ostream>

class Vector4 {

public:
    double x;
    double y;
    double z;
    double w;

    Vector4();
    Vector4(double x, double y, double z);
    Vector4(double x, double y, double z, double w);

    virtual ~Vector4();

    Vector4& operator=(Vector4 const&v);
    double & operator[](int i);

    friend bool operator==(const Vector4 &lhs, const Vector4 &rhs);
    friend bool operator!=(const Vector4 &lhs, const Vector4 &rhs);

    friend std::ostream &operator<<(std::ostream &os, const Vector4 &vector4);
    friend Vector4 operator-(const Vector4 &lhs, const Vector4 &rhs);
    friend Vector4 operator+(const Vector4 &lhs, const Vector4 &rhs);
    friend Vector4 operator*(const double &lhs, const Vector4 &rhs);
    friend Vector4 operator*(const Vector4 &lhs, const double &rhs);
    friend Vector4 operator/(const Vector4 &lhs, const double &rhs);

    static double Dot(const Vector4 &dhs, const Vector4 &rhs);
    static Vector4 Cross(const Vector4 &dhs, const Vector4 &rhs);
    static Vector4 Normalize(const Vector4 &v);
    static double Distance(const Vector4 &lhs, const Vector4 &rhs);

    Vector4 operator-() const;
    double sqr() const;
    double mag() const;

};

#endif //INC_3DMOTOR_VECTOR4_H