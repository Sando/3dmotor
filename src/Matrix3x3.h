//
// Created by jamesrapha on 29/08/2020.
//

#ifndef INC_3DMOTOR_MATRIX3X3_H
#define INC_3DMOTOR_MATRIX3X3_H


#include "vector3.h"

class Matrix3x3 {

public:
    Vector3 X;
    Vector3 Y;
    Vector3 Z;

    Matrix3x3();
    Matrix3x3(Vector3 X, Vector3 Y, Vector3 Z);
    Matrix3x3(double x1, double x2, double x3,
              double y1, double y2, double y3,
              double z1, double z2, double z3);

    virtual ~Matrix3x3();

    Vector3 & operator[](int i);

    bool operator==(const Matrix3x3 &rhs) const;
    bool operator!=(const Matrix3x3 &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Matrix3x3 &matrix3x3);

    friend Matrix3x3 operator+(const Matrix3x3 &lhs, const Matrix3x3 &rhs);
    friend Matrix3x3 operator-(const Matrix3x3 &lhs, const Matrix3x3 &rhs);

    friend Matrix3x3 operator*(const double &lhs, const Matrix3x3 &rhs);
    friend Matrix3x3 operator*(const Matrix3x3 &lhs, const double &rhs);
    friend Vector3 operator*(const Vector3 &lhs, const Matrix3x3 & rhs);
    friend Matrix3x3 operator/(const Matrix3x3 &lhs, const double &rhs);

    double det() const;

    static Matrix3x3 Zero();
    static Matrix3x3 Identity();

    static Matrix3x3 Transpose(const Matrix3x3& m);
};


#endif //INC_3DMOTOR_MATRIX3X3_H
