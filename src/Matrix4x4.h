//
// Created by jamesrapha on 29/08/2020.
//

#ifndef INC_3DMOTOR_MATRIX4X4_H
#define INC_3DMOTOR_MATRIX4X4_H


#include "vector4.h"

class Matrix4x4 {

public:
    Vector4 X;
    Vector4 Y;
    Vector4 Z;
    Vector4 W;

    Matrix4x4();
    Matrix4x4(const Vector4& X, const Vector4& Y, const Vector4& Z, const Vector4& W);
    Matrix4x4(double x1, double x2, double x3, double x4,
              double y1, double y2, double y3, double y4,
              double z1, double z2, double z3, double z4,
              double w1, double w2, double w3, double w4);



    virtual ~Matrix4x4();

    Vector4 & operator[](int i);

    bool operator==(const Matrix4x4 &rhs) const;
    bool operator!=(const Matrix4x4 &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Matrix4x4 &matrix4x4);

    friend Matrix4x4 operator+(const Matrix4x4 &lhs, const Matrix4x4 &rhs);
    friend Matrix4x4 operator-(const Matrix4x4 &lhs, const Matrix4x4 &rhs);

    friend Matrix4x4 operator*(const double &lhs, const Matrix4x4 &rhs);
    friend Matrix4x4 operator*(const Matrix4x4 &lhs, const double &rhs);
    friend Vector4 operator*(const Vector4 &lhs, const Matrix4x4 & rhs);
    friend Matrix4x4 operator/(const Matrix4x4 &lhs, const double &rhs);

    double det() const;

    static Matrix4x4 Zero();
    static Matrix4x4 Identity();

    static Matrix4x4 Transpose(const Matrix4x4& m);
};


#endif //INC_3DMOTOR_MATRIX4X4_H
